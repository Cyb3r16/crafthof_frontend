import {Component, Input, OnInit} from '@angular/core';
import {BackendService} from '../services/backend.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {S3UploadService} from '../services/s3-upload.service';
import {Rating} from '../classes/Rating';
import {ErrorDialogComponent} from '../error-dialog/error-dialog.component';
import {OverallRating} from '../classes/OverallRating';
import {Observable} from 'rxjs/Observable';
import {forEach} from '@angular/router/src/utils/collection';
import {ProductService} from '../services/product.service';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  @Input() productId: string;

  model = new Rating();
  overallRating$: Observable<OverallRating>;

  addRatingViewHidden = true;

  constructor(private backendService: BackendService,
              private dialog: MatDialog,
              private s3Upload: S3UploadService,
              private productService: ProductService) {

    this.overallRating$ = this.productService.getOverallRating$();
  }

  ngOnInit() {
    this.productService.getOverallRatingFromId(this.productId);
    this.model.productId = this.productId;
    this.model.rating = 1;
  }

  onSubmit() {
    this.backendService.addRating(this.model, (res, error) => {
      if (error) {
        this.openDialog(error.message);
      } else {
        this.productService.refreshRatings(this.productId);
        this.addRatingViewHidden = true;
        this.model = new Rating();
        this.model.rating = 1;
        this.model.productId = this.productId;
      }
    });
  }

  public starClicked(starClicked: number) {
    this.model.rating = starClicked;
  }

  public addRatingClicked() {
    this.addRatingViewHidden = false;
  }

  private openDialog(error: string) {
    const dialogRef = this.dialog.open(ErrorDialogComponent, {
      data: error
    });
  }

  public round(value, precision) {

    const multiplier = Math.pow(10, precision || 0);
    const result = Math.floor(value * multiplier) / multiplier;
    if (isNaN(result)) {
      return 0;
    } else {
      return result;
    }
  }

}
