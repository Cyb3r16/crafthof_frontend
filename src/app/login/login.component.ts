import {Component, OnDestroy} from '@angular/core';
import {BackendService} from '../services/backend.service';
import {User} from '../classes/User';
import {MatDialog} from '@angular/material';
import {ErrorDialogComponent} from '../error-dialog/error-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {

  model = new User();
  rememberMe: boolean;

  loggedIndSub: Subscription;

  constructor(private backendService: BackendService, private errorDialog: MatDialog,
              private router: Router, private userService: UserService) {
    if (this.router.url.includes('/profile')) {
      this.loggedIndSub = this.userService.getLoggedIn$().subscribe( res => {
        if (res) {
          this.router.navigateByUrl('/profile');
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this.loggedIndSub) {
      this.loggedIndSub.unsubscribe();
    }
  }

  onSubmit() {
    this.backendService.login(this.model, (res, error) => {
      if (error) {
        this.errorDialog.open(ErrorDialogComponent, {
          data: error.message
        });
      } else {
        this.userService.setLoggedIn(true);
        if (this.router.url === '/profile/login') {
          this.router.navigateByUrl('/profile');
        } else {
          this.router.navigateByUrl('/');
        }
      }
    }, this.rememberMe);
  }

}
