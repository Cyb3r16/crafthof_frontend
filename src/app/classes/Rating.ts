
export class Rating {

  userName: string;
  userId: string;
  productId: string;
  title: string;
  text: string;
  rating: number;
  createdAt: string;


  public formatCreationDate(createdAt: string): string {
    const date = new Date(createdAt);
    return date.getDate().toString();
  }

}



