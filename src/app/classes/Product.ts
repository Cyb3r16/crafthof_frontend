export class Product {

  _id: string;
  name: string;
  shopId: number;
  shopName: string;
  text: string;
  category: number;
  keywords: Array<string>;
  img: string;
  likes: number;
  price: number;
  currency: string;
  ratingAvg: number;
  ratingCount: number;
}
