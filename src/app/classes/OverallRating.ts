export class OverallRating {

  average: number;
  countOfRatings: number;
  ratingStat: any[]; //  { "_id": 5, "countOfRatingLevel": 2 }

}
