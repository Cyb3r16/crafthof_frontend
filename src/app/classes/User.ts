import {Shop} from './Shop';

export class User {

  _id: number;
  name: string;
  email: string;
  password: string;
  passwordConf?: string;
  phone?: string;
  address?: string;
  shop?: Shop;
  titleImg: string;
  profileImg: string;
  follows: string[];
  likes: string[];
}
