import {Product} from './Product';

export class Shop {

  _id: number;
  name: string;
  shopEmail: string;
  phone: string;
  bio: string;
  website: string;
  shopAddress: string;
  shopLatitude: number;
  shopLongitude: number;
  titleImg: string;
  profileImg: string;
  products: string[];
}
