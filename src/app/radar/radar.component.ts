import { Component, OnInit } from '@angular/core';
import {GeoCoords} from '../classes/GeoCoords';
import {Observable} from 'rxjs/Observable';
import {Shop} from '../classes/Shop';
import {BackendService} from '../services/backend.service';
import {MatDialog} from '@angular/material';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-radar',
  templateUrl: './radar.component.html',
  styleUrls: ['./radar.component.scss']
})
export class RadarComponent implements OnInit {

  localLat: number;
  localLng: number;

  shopsInRadius$ = new Subject<Shop[]>();
  radius = 10000;

  img = 'https://s3.eu-central-1.amazonaws.com/crafthof-photos/thejamcreator.jpg';

  constructor(private backendService: BackendService,
              private dialog: MatDialog) { }

  ngOnInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        // console.log(position.coords.latitude);
        this.localLat = position.coords.latitude;
        this.localLng = position.coords.longitude;
        this.backendService.getShopsInRadius(this.radius, position.coords.latitude, position.coords.longitude)
          .subscribe(res => this.shopsInRadius$.next(res));
      });
    } else {
      console.log('Geolocation is not supported by this browser.');
    }
  }

  public onRadiusChanged(event) {
    console.log(event);
    if (event.target == null) {
      if ( Math.round(Number(event) / 1000) > 0 ) {
        console.log('circle radius changed', Math.round(Number(event) / 1000));
        this.radius = Math.round(Number(event) / 1000) * 1000;
      }
    } else {
      console.log('1onRadiusChanged', event.target.value);
      this.radius = Number(event.target.value) * 1000;
    }
    this.backendService.getShopsInRadius(this.radius, this.localLat, this.localLng)
      .subscribe(res => this.shopsInRadius$.next(res));
  }

}
