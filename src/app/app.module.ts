import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { EqualValidatorDirective } from './register/equal-validator.directive';
import { LoginComponent } from './login/login.component';

import { UserService } from './services/user.service';
import { BackendService } from './services/backend.service';

import {
  MatButtonModule, MatCardModule, MatDialogModule, MatIconModule, MatMenuModule,
  MatToolbarModule
} from '@angular/material';

import { AgmCoreModule } from '@agm/core';

import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import {CredentialsInterceptor} from './interceptor/credentials.interceptor';
import { ProductsComponent } from './products/products.component';
import { AddProductComponent } from './add-product/add-product.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './profile/profile.component';
import { ProductDescriptionComponent } from './product-description/product-description.component';
import { ProductService } from './services/product.service';
import { S3UploadService } from './services/s3-upload.service';
import { ReviewComponent } from './review/review.component';
import { RatingComponent } from './rating/rating.component';
import { RadarComponent } from './radar/radar.component';
import { RadarService } from './services/radar.service';
import { CategoriesComponent } from './categories/categories.component';

export const ROUTES: Routes = [
  { path: '', component: ProductsComponent },
  { path: 'register/:type', component: RegisterComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'products/productPage', component: ProductDescriptionComponent },
  { path: 'addProduct', component: AddProductComponent },
  { path: 'editProduct/:productId', component: AddProductComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'productPage/:productId', component: ProductDescriptionComponent },
  { path: 'review', component: ReviewComponent },
  { path: 'rating', component: RatingComponent },
  { path: 'profile/login', component: LoginComponent },
  { path: 'productPage/:productId', component: ProductDescriptionComponent },
  { path: 'shop/:shopId', component: ProfileComponent},
  { path: 'radar', component: RadarComponent },
  { path: 'search', component: ProductsComponent }


];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    EqualValidatorDirective,
    LoginComponent,
    ErrorDialogComponent,
    ProductsComponent,
    AddProductComponent,
    FooterComponent,
    HeaderComponent,
    ProfileComponent,
    ProductDescriptionComponent,
    ReviewComponent,
    RatingComponent,
    RadarComponent,
    CategoriesComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(ROUTES),
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAcx5033LCOR6k0W50pp5Dw6f56ppppd_4'
    })
  ],
  providers: [
    S3UploadService,
    UserService,
    RadarService,
    BackendService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CredentialsInterceptor ,
      multi: true
    },
    ProductService
  ],
  entryComponents: [
    ErrorDialogComponent
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
