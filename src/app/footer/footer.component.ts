import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  constructor(private router: Router, private userService: UserService) { }

  toolBarClicked(icon: number) {
    let url = '/';
    switch (icon) {
      case 1:
        url = '/search';
        break;
      case 2:
        url = '/radar';
        break;
      case 3:
        if (this.userService.getLoggedIn()) {
          url = '/profile';
        } else {
          url = '/profile/login';
        }

        break;
    }
    this.router.navigateByUrl(url);
  }
}
