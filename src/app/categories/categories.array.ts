export const   categories: any = [
  {
    'key': '0',
    'value': 'All'
  },
  {
    'key': '1',
    'value': 'Art'
  },
  {
    'key': '2',
    'value': 'Beauty'
  },
  {
    'key': '3',
    'value': 'Decoration'
  },
  {
    'key': '4',
    'value': 'Entertainment'
  },
  {
    'key': '5',
    'value': 'Fashion'
  },
  {
    'key': '6',
    'value': 'Foods'
  },
  {
    'key': '7',
    'value': 'Garden'
  },
  {
    'key': '8',
    'value': 'Living'
  },
  {
    'key': '9',
    'value': 'Manufacture'
  },
  {
    'key': '10',
    'value': 'Technology'
  },
  {
    'key': '11',
    'value': 'Other'
  }
];
