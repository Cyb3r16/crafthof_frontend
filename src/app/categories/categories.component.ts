import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProductService} from '../services/product.service';
import { categories } from './categories.array';
import 'rxjs/add/operator/skip';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories = categories;
  categorySelected = 0;

  isHome: boolean;

  constructor(private productService: ProductService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.categorySelected = 0;
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (location.pathname === '/' || location.pathname === '/search') {
        this.isHome = true;
      } else {
        this.isHome = false;
      }
    });
    this.productService.getCategory$().next(0);
    // subscribe to router event
    this.activatedRoute.queryParams
      .skip(1)
      .subscribe((params: Params) => {
      if (params['category']) {
        this.categorySelected = params['category'];
        this.productService.getCategory$().next(this.categorySelected);
      }
    });
  }

  public onCategoryClicked(category: number) {
    this.categorySelected = category;
    this.router.navigate([''], { queryParams: { category: this.categorySelected } });
    this.productService.getCategory$().next(this.categorySelected);
  }

}
