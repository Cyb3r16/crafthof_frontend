import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../classes/User';
import {Observable} from 'rxjs/Observable';
import {Product} from '../classes/Product';
import {ActivatedRoute, Router} from '@angular/router';
import {Shop} from '../classes/Shop';
import {BackendService} from '../services/backend.service';
import {Subscription} from 'rxjs/Subscription';
import {ProductService} from '../services/product.service';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {S3UploadService} from '../services/s3-upload.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnDestroy {

  user$: Observable<User>;
  shop$: Observable<Shop>;
  products$: Observable<Product[]>;
  wishlist$ = new ReplaySubject<Product[]>();

  shopId: string;
  getLoggedInSubscription: Subscription;
  tabSelected = 0;
  selectedProduct: string;
  isPrivate = false;
  isShop = false;

  constructor(private userService: UserService,
              private productService: ProductService,
              private router: Router,
              route: ActivatedRoute,
              private backendService: BackendService,
              private s3Upload: S3UploadService) {
    if (router.url.indexOf('shop/') >= 0) {
      route.params.subscribe(params => {
        this.shop$ = backendService.getShop(params['shopId']);
        this.products$ = backendService.getProductsFromShop(params['shopId']);
      });
    } else {
      this.backendService.getWishlist().subscribe(products => this.wishlist$.next(products));
      this.isPrivate = true;
      this.user$ = this.userService.getUser$();
      this.products$ = this.userService.getProductsFromShop();

      if (this.userService.getUser() && this.userService.getUser().shop) {
        this.userService.updateProductsFromShop(this.userService.getUser().shop._id);
      }

      this.getLoggedInSubscription = this.userService.getLoggedIn$().subscribe(res => {
        if (!res && router.url.includes('profile')) {
          this.router.navigateByUrl('/profile/login');
        } else if (router.url.includes('profile')) {
          this.router.navigateByUrl('/profile');
          this.isShop = this.userService.isShop;
          (this.isShop) ? this.tabSelected = 0 : this.tabSelected = 1;
          this.onTabSelected(this.tabSelected);
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this.getLoggedInSubscription) {
      this.getLoggedInSubscription.unsubscribe();
    }
  }

  public onTabSelected(index: number) {
    switch (index) {
      case 0:
        this.tabSelected = 0;
        this.products$ = this.userService.getProductsFromShop();
        break;
      case 1:
        this.tabSelected = 1;
        this.products$ = this.wishlist$;
        break;
    }
  }

  public onSelectProduct(productId: string): void {
    if (this.isPrivate) {
      if (this.selectedProduct !== productId) {
        this.selectedProduct = productId;
      } else {
        this.selectedProduct = '';
      }
    } else {
      this.router.navigateByUrl( 'productPage/' + productId);
    }

  }

  public onFloatButtonClicked(index: number) {
    switch (index) {
      case 0:
        if (this.selectedProduct) {
          this.router.navigateByUrl('/editProduct/' + this.selectedProduct);
        }
        break;
      case 1:
        this.router.navigateByUrl('/addProduct');
        break;
      case 2:
        if (this.isShop && this.tabSelected === 0) {
          this.backendService.deleteProduct(this.selectedProduct, (res, error) => {
            if (error) {
              console.log(error);
            } else {
              this.selectedProduct = '';
              this.userService.updateProductsFromShop(this.userService.getUser().shop._id);
            }
          });
        } else {
          this.backendService.unlikeProduct(this.selectedProduct, (res, error) => {
            if (error) {
              console.log(error.message);
            } else {
              this.backendService.getWishlist().subscribe(products => this.wishlist$.next(products));
            }
          });
        }
        break;
    }
  }

  public fileEvent(fileInput: any) {
    if (this.userService.getUser()) {
      const url = this.s3Upload.fileEvent(fileInput);
      let data;
      if (this.isShop) {
         data = {
          shopProfileImg: url
        };
      } else {
         data = {
          userProfileImg: url
        };
      }
      this.backendService.updatePicture(data, (res, err) => {
        if (err) {
          console.log('Error', err);
        } else {
          console.log('Res', res);
        }
      });
    }


  }

}
