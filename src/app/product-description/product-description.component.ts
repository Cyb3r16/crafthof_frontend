import {Component, Input, OnInit} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {Product} from '../classes/Product';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProductService} from '../services/product.service';
import {Observable} from 'rxjs/Observable';
import {BackendService} from '../services/backend.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-product-description',
  templateUrl: './product-description.component.html',
  styleUrls: ['./product-description.component.scss']
})
export class ProductDescriptionComponent implements OnInit {

  productId: string;
  product$: Observable<Product>;

  public isLiked: boolean;
  @Input() likeButtonIcon = 'heart_empty';

  public isAddedToShoppingCart: boolean;
  @Input() shoppingCartButtonIcon = 'shopping_cart_add';

  constructor(private activatedRoute: ActivatedRoute, private productService: ProductService, iconRegistry: MatIconRegistry,
              sanitizer: DomSanitizer, private backendService: BackendService, private userService: UserService,
              private router: Router) {
    iconRegistry.addSvgIcon(
      'heart_empty',
      sanitizer.bypassSecurityTrustResourceUrl('assets/heart_empty.svg'));
    iconRegistry.addSvgIcon(
      'heart_filled',
      sanitizer.bypassSecurityTrustResourceUrl('assets/heart_filled.svg'));

    iconRegistry.addSvgIcon(
      'shopping_cart_add',
      sanitizer.bypassSecurityTrustResourceUrl('assets/shopping_cart_add.svg'));
    iconRegistry.addSvgIcon(
      'shopping_cart_filled',
      sanitizer.bypassSecurityTrustResourceUrl('assets/shopping_cart_filled.svg'));

    this.product$ = this.productService.getProduct$();
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.productId = params['productId'];
      this.productService.getProductById(params['productId']);
    });
  }

  public changeLikeIcon(event) {
    event.stopPropagation();
    console.log('event_changeLikeIcon', event);
    if (this.userService.getUser()) {
      for (const id of this.userService.getUser().likes) {
        if ( this.productId === id) {
          this.backendService.unlikeProduct(this.productId, (res, error) => {
            if (error) {
              console.log(error.message);
              return 'heart_filled';
            } else {
              this.userService.setNewUserAfterLike(res);
              return 'heart_empty';
            }
          });
        }
      }
      this.backendService.likeProduct(this.productId, (res, error) => {
        if (error) {
          console.log(error.message);
          return 'heart_empty';
        } else {
          this.userService.setNewUserAfterLike(res);
          return 'heart_filled';
        }
      });
    } else if (!this.userService.getLoggedIn()) {
      this.router.navigateByUrl('/login');
    }
  }

  public getLikeIcon(): string {
    if (this.userService.getUser()) {
      for (const id of this.userService.getUser().likes) {
        if ( this.productId && this.productId === id) {
          return 'heart_filled';
        }
      }
      return 'heart_empty';
    }
    return 'heart_empty';
  }

}
