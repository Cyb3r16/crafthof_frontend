import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {Product} from '../classes/Product';
import {BackendService} from '../services/backend.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {ProductService} from '../services/product.service';
import {User} from '../classes/User';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit{

  user: User;
  user$: Observable<User>;
  products$: Observable<Product[]>;

  @Input() likeButtonIcon = 'heart_empty';
  @Input() shoppingCartButtonIcon = 'shopping_cart_add';

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,
              private backendService: BackendService,
              private router: Router,
              private productService: ProductService,
              private userService: UserService ) {
    iconRegistry.addSvgIcon(
      'heart_empty',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/heart_empty_red.svg'));
    iconRegistry.addSvgIcon(
      'heart_filled',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons/heart_filled_red.svg'));

    iconRegistry.addSvgIcon(
      'shopping_cart_add',
      sanitizer.bypassSecurityTrustResourceUrl('assets/shopping_cart_add.svg'));
    iconRegistry.addSvgIcon(
      'shopping_cart_filled',
      sanitizer.bypassSecurityTrustResourceUrl('assets/shopping_cart_filled.svg'));

    this.user$ = this.userService.getUser$();
    this.user$.subscribe(user => this.user = user );
    this.products$ = this.productService.getProducts$();
  }

  ngOnInit(): void {
    this.productService.getCategory$().next(this.productService.getCategory());
    this.userService.updateUser();
  }

  public getLikeIcon(productId: string): string {
    if (this.user) {
      for (const id of this.user.likes) {
        if ( productId === id) {
          return 'heart_filled';
        }
      }
      return 'heart_empty';
    }
    return 'heart_empty';
  }

  public changeLikeIcon(event, productId: string) {
    event.stopPropagation();
    console.log('event_changeLikeIcon', event);
    if (this.user) {
      for (const id of this.user.likes) {
        if ( productId === id) {
          this.backendService.unlikeProduct(productId, (res, error) => {
            if (error) {
              console.log(error.message);
              return 'heart_filled';
            } else {
              this.userService.setNewUserAfterLike(res);
              return 'heart_empty';
            }
          });
        }
      }
      this.backendService.likeProduct(productId, (res, error) => {
        if (error) {
          console.log(error.message);
          return 'heart_empty';
        } else {
          this.userService.setNewUserAfterLike(res);
          return 'heart_filled';
        }
      });
    } else if (!this.userService.getLoggedIn()) {
      this.router.navigateByUrl('/login');
    }
  }

  public productClicked(event, index: any) {
    console.log('event', event);
    this.router.navigate(['/productPage/', this.productService.getProducts()[index]._id]);
  }

}
