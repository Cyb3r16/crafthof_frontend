import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, ResolveEnd, Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {Observable} from 'rxjs/Observable';
import {User} from '../classes/User';
import {BackendService} from '../services/backend.service';
import {ErrorDialogComponent} from '../error-dialog/error-dialog.component';
import * as algoliasearch from 'algoliasearch';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import {Product} from '../classes/Product';
import {ProductService} from '../services/product.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() crafthofIcon = 'crafthof_logo';
  isLoggedIn: Observable<Boolean>;

  isHome: boolean;

  searchString: string;

  algoliaClient: any;
  algoliaIndex: any;

  category: number;

  pageActive = 0;

  public searchSubject$ = new Subject<string>();

  isSearch = false;

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private router: Router,
              private userService: UserService, private backendService: BackendService,
              private errorDialog: MatDialog, private productService: ProductService) {
    this.isLoggedIn = this.userService.getLoggedIn$();

    this.productService.getCategory$().subscribe(cat => {
      this.category = cat;
      if (this.searchString && this.searchString !== '') {
        this.searchSubject$.next(this.searchString);
      }
    });
  }

  ngOnInit() {
    this.algoliaClient = algoliasearch('WIOZRBG2I6', 'e963f63301ce460576934ecb2aedd47c');
    this.algoliaIndex = this.algoliaClient.initIndex('products');
    this.initSearch();
    this.router.events.subscribe(event => {
        if (location.pathname === '/') {
          this.isHome = true;
        } else {
          this.isHome = false;
        }
        if (location.pathname.includes('profile')) {
          this.pageActive = 4;
        } else if (location.pathname.includes('radar')) {
          this.pageActive = 3;
        } else {
          this.pageActive = 0;
        }
        if (location.pathname.includes('search')) {
          this.isSearch = true;
        } else {
          if (this.isSearch) {
            this.searchString = '';
            this.searchSubject$.next(this.searchString);
            this.isSearch = false;
          }
        }
    });
  }

  ngOnDestroy(): void {
    this.searchSubject$.unsubscribe();
  }

  public logout() {
    this.backendService.logout((res, error) => {
      if (error) {
        this.errorDialog.open(ErrorDialogComponent, {
          data: error.message
        });
      } else {
        this.userService.setLoggedIn(false);
      }
    });
  }

  public goTo(index: number) {
    let url = '/';
    switch (index) {
      case 1:
        this.router.navigateByUrl('/register');
        break;
      case 2:
        this.router.navigateByUrl('/login');
        break;
      case 3:
        this.router.navigateByUrl('/radar');
        break;
      case 4:
        if (this.userService.getLoggedIn()) {
          url = '/profile';
        } else {
          url = '/profile/login';
        }
        this.router.navigateByUrl(url);
        break;
      case 5:
        this.router.navigateByUrl('/');
        break;
    }
  }

  private initSearch() {

    this.searchSubject$
      .debounceTime(400)
      .subscribe(searchValue => {
        this.searchString = searchValue;
        if (searchValue !== '') {
          this.productService.setSearchActive(true);
          const that = this;
          let filter;
          if (!this.category || this.category == 0) {
            filter = undefined;
          } else {
            filter = 'category = ' + this.category;
          }
          this.algoliaIndex
            .search({
              query: searchValue,
              filters: filter,
              hitsPerPage: 100
            })
            .then(function (content) {
              const products = new Array<Product>();
              content.hits.forEach( hit => {
                products.push({
                  _id: hit.objectID,
                  name: hit.name,
                  shopId: hit.shopId,
                  shopName: hit.shopName,
                  text: hit.text,
                  category: hit.category,
                  img: hit.img,
                  likes: hit.likes,
                  price: hit.price,
                  keywords: undefined,
                  currency: undefined,
                  ratingAvg: hit.ratingAvg,
                  ratingCount: hit.ratingCount
                });
              });
              that.productService.setSearchProducts(products);
            })
            .catch(function (err) {
              console.error(err);
            });
        } else {
          this.productService.setSearchActive(false);
          this.productService.getCategory$().next(this.category);
        }
    });
  }
}
