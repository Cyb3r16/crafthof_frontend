import { Component, OnInit } from '@angular/core';
import {Product} from '../classes/Product';
import {BackendService} from '../services/backend.service';
import {MatDialog} from '@angular/material';
import {ErrorDialogComponent} from '../error-dialog/error-dialog.component';
import {S3UploadService} from '../services/s3-upload.service';
import {categories} from '../categories/categories.array';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProductService} from '../services/product.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  model = new Product();
  submitted = false;
  selectedProductId: string;
  product$: Observable<Product>;
  inEditMode: boolean;

  categories = categories.slice(0);

  constructor(private backendService: BackendService,
              private dialog: MatDialog, private s3Upload: S3UploadService,
              private router: Router,
              private userService: UserService,
              private activatedRoute: ActivatedRoute,
              private productService: ProductService) {
    this.categories.splice(0, 1); // Delete All-Category

    console.log(router.url);
    if (router.url.includes('edit')) {
      this.inEditMode = true;
    }
    if (router.url.includes('add')) {
      this.inEditMode = false;
    }

    this.product$ = this.productService.getProduct$();
    this.product$.subscribe(product => this.model = product);
  }

  ngOnInit() {
    if (this.inEditMode) {
      this.activatedRoute.params.subscribe((params: Params) => {
        this.selectedProductId = params['productId'];
        this.productService.getProductById(params['productId']);

      });
    }
  }

  onSubmit() {
    if (this.inEditMode) {
      this.backendService.updateProduct(this.model, (res, error) => {
        let url = '/';
        if (error) {
          this.openDialog(error.message);
        } else {
          if (this.userService.getLoggedIn()) {
            url = '/profile';
          } else {
            url = '/profile/login';
          }
          this.router.navigateByUrl(url);
        }
      });
    } else {
      this.backendService.addProduct(this.model, (res, error) => {
        let url = '/';
        if (error) {
          this.openDialog(error.message);
        } else {
          if (this.userService.getLoggedIn()) {
            url = '/profile';
          } else {
            url = '/profile/login';
          }
          this.router.navigateByUrl(url);
        }
      });
    }
  }

  private openDialog(error: string) {
    const dialogRef = this.dialog.open(ErrorDialogComponent, {
      data: error
    });
  }

  public fileEvent(fileInput: any) {
    this.model.img = this.s3Upload.fileEvent(fileInput);
  }

}
