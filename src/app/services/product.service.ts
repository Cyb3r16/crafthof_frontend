import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Product} from '../classes/Product';
import {BackendService} from './backend.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Rating} from '../classes/Rating';
import {Subject} from 'rxjs/Subject';
import {OverallRating} from '../classes/OverallRating';
import {Subscription} from 'rxjs/Subscription';

@Injectable()
export class ProductService {

  private products$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>(new Array<Product>());
  private products = new Array<Product>();
  private category$: Subject<number> = new Subject<number>();
  private category = 0;

  private isSearchActive = false;

  private productsRequest: Subscription;

  private product$: Subject<Product> = new Subject<Product>();
  private ratings$: Subject<Rating[]> = new Subject<Rating[]>();
  private overallRating$: Subject<OverallRating> = new Subject<OverallRating>();

  constructor(private backendService: BackendService) {
    this.category$.subscribe( res => this.getProductsFromCategory(res));
    this.products$.subscribe( res => this.products = res);
  }


  private getMoreProductsFromServer(): Subscription {
    return this.backendService.getProducts().subscribe( products =>  this.products$.next(products));
  }

  public getProducts(): Product[] {
    return this.products;
  }

  public getProducts$(): Observable<Product[]> {
    return this.products$;
  }

  public getRatings$(): Observable<Rating[]> {
    return this.ratings$;
  }

  public getOverallRating$(): Observable<OverallRating> {
    return this.overallRating$;
  }

  public getProduct$(): Observable<Product> {
    return this.product$;
  }

  public getProductById(id: string) {
    const product = this.products.find( obj => obj._id === id);
    if (product) {
      setTimeout(() => this.product$.next(product), 1);
    } else {
      this.backendService.getProductById(id).subscribe(res => this.product$.next(res));
    }
  }

  public refreshRatings(productId: string) {
    this.getRatingsFromId(productId);
    this.getOverallRatingFromId(productId);
    this.backendService.getProductById(productId).subscribe(res => this.product$.next(res));
  }

  public getRatingsFromId(productId: string) {
     this.backendService.getRatings(productId).subscribe( res => this.ratings$.next(res));
  }

  public getOverallRatingFromId(productId: string) {
    this.backendService.getOverallRating(productId).subscribe( res => this.overallRating$.next(res));
  }

  public getCategory$() {
    return this.category$;
  }

  private getProductsFromCategory(category: number) {
    this.category = category;
    if (!this.isSearchActive) {
      if (this.productsRequest) {
        this.productsRequest.unsubscribe();
      }
      if (category == 0) {
        this.productsRequest = this.getMoreProductsFromServer();
      } else {
        this.productsRequest = this.backendService.getProductsFromCategory(category).subscribe(res => this.products$.next(res));
      }
    }
  }

  public setSearchProducts(products: Product[]) {
    this.products$.next(products);
  }

  public setSearchActive(state: boolean) {
    this.isSearchActive = state;
  }

  public getCategory(): number {
    return this.category;
  }
}
