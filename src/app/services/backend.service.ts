import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions} from '@angular/http';
import {User} from '../classes/User';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import {Product} from '../classes/Product';
import {Observable} from 'rxjs/Observable';
import {Shop} from '../classes/Shop';
import {Rating} from '../classes/Rating';
import {OverallRating} from '../classes/OverallRating';

@Injectable()
export class BackendService {

  private readonly GM_API_KEY = 'AIzaSyAcx5033LCOR6k0W50pp5Dw6f56ppppd_4';
  private readonly url = 'http://crafthof.eu-central-1.elasticbeanstalk.com/';
  private readonly localUrl = 'http://localhost:8081/';
  private readonly local = true;
  private readonly urlGMGeocoding = 'https://maps.googleapis.com/maps/api/geocode/json?address=';

  constructor(private http: HttpClient) {}

  private getUrl(): string {
    if (this.local) {
      return this.localUrl;
    } else {
      return this.url;
    }
  }

  private getUrlGMGeocoding(): string {
    return this.urlGMGeocoding;
  }

  public geocodeAddress(address: string, callback: (res: any, error?: any) => void) {
    this.doGetGMGeocoding(address, callback);
  }

  public createUser(user: User, callback: (res: any, error?: any) => void) {
    const data: any = {
      name: user.name,
      email: user.email,
      password: user.password,
      phone: user.phone,
      address: user.address,
      titleImg: user.titleImg,
      profileImg: user.profileImg,
    };
    if (user.shop) {
      data.shopName = user.shop.name;
      data.shopAddress = user.shop.shopAddress;
      console.log('location');
      console.log(user.shop.shopLongitude);
      console.log(user.shop.shopLatitude);
      data.lat = user.shop.shopLatitude;
      data.long = user.shop.shopLongitude;
      data.website = user.shop.website;
      data.bio = user.shop.bio;
    }
    this.doPost('user/createUser', data, callback);
  }

  public login(user: User, callback: (res: any, error?: any) => void, rememberMe?: boolean) {
    const data = {
      email: user.email,
      password: user.password,
      rememberMe: rememberMe
    };
    this.doPost('auth/login', data, callback);
  }

  public updateProduct(product: Product, callback: (res: any, error?: any) => void) {
    const data = {
      name: product.name,
      text: product.text,
      price: product.price,
      img: product.img,
      category: product.category
    };
    this.doPut('product/' + product._id, data, callback);
  }

  public updatePicture(data, callback: (res: any, error?: any) => void) {
    this.doPut('user/', data, callback);
  }

  public addProduct(product: Product, callback: (res: any, error?: any) => void ) {
    const data = {
      name: product.name,
      text: product.text,
      price: product.price,
      img: product.img,
      category: product.category
    };
    this.doPost('product', data, callback);
  }

  public deleteProduct(productId: string, callback: (res: any, error?: any) => void) {
    this.doDelete('product/' + productId, callback);
  }

  public likeProduct(productId: string, callback: (res: any, error?: any) => void ) {
    const data = {
      productId: productId
    };
    this.doPost('product/subscribe', data, callback);
  }

  public unlikeProduct(productId: string, callback: (res: any, error?: any) => void ) {
    const data = {
      productId: productId
    };
    this.doPost('product/unsubscribe', data, callback);
  }

  public logout(callback: (res: any, error?: any) => void) {
    this.doGet('auth/logout', callback);
  }

  public getLoggedInUser(): Observable<User> {
    return this.http.get<User>(this.getUrl() + 'auth/getLoggedInUser');
  }

  public getProductsFromShop(shopId: number): Observable<Product[]> {
    return this.http.get<Product[]>(this.getUrl() + 'shop/productsFromShop/' + shopId + '/100');
  }

  public getShopsInRadius(radius: number, latitude: number, longitude: number): Observable<Shop[]> {
    return this.http.get<Shop[]>(this.getUrl() + 'shop/getNearShops?lat=' + latitude + '&long=' + longitude + '&radius=' + radius);
  }

  public getShop(shopId: string): Observable<Shop> {
    return this.http.get<Shop>(this.getUrl() + 'shop/getShop/' + shopId);
  }

  public getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.getUrl() + 'product/getPage');
  }

  public getProductsFromCategory(category: number): Observable<Product[]> {
    return this.http.get<Product[]>(this.getUrl() + 'product/getProductsFromCategory/?category=' + category);
  }

  public getProductById(id: string) {
    return this.http.get<Product>(this.getUrl() + 'product/productById/' + id);
  }

  public getWishlist(): Observable<Product[]> {
    return this.http.get<Product[]>(this.getUrl() + 'user/getWishlist');
  }

  public getRatings(productId: string): Observable<Rating[]> {
    return this.http.get<Rating[]>(this.getUrl() + 'rating/ratingsFromProduct/' + productId);
  }

  public getOverallRating(productId: string): Observable<OverallRating> {
    return this.http.get<OverallRating>(this.getUrl() + 'rating/ratingStatsFromProduct/' + productId);
  }

  public addRating(rating: Rating, callback: (res: any, error?: any) => void ) {
    const data = {
      productId: rating.productId,
      text: rating.text,
      title: rating.title,
      rating: rating.rating
    };
    this.doPost('rating', data, callback);
  }

  private doPost(path: string, data: any, callback: (res: any, error?: any) => void, numberOfRetrys: number = 0) {
    this.http.post(this.getUrl() + path, data)
      .retry(numberOfRetrys)
      .subscribe(
      res => callback(res),
      (err: HttpErrorResponse) => callback(undefined, err.error)
    );
  }

  private doGet(path: string, callback: (res: any, error?: any) => void, numberOfRetrys: number = 0) {
    this.http.get(this.getUrl() + path)
      .retry(numberOfRetrys)
      .subscribe(
        res => callback(res),  // function
        (err: HttpErrorResponse) => callback(undefined, err.error)
      );
  }

  private doDelete(path: string, callback: (res: any, error?: any) => void, numberOfRetrys: number = 0) {
    this.http.delete(this.getUrl() + path)
      .retry(numberOfRetrys)
      .subscribe(
        res => callback(res),  // function
        (err: HttpErrorResponse) => callback(undefined, err.error)
      );
  }

  private doPut(path: string, data: any, callback: (res: any, error?: any) => void, numberOfRetrys: number = 0) {
    this.http.put(this.getUrl() + path, data)
      .retry(numberOfRetrys)
      .subscribe(
        res => callback(res),
        (err: HttpErrorResponse) => callback(undefined, err.error)
      );
  }

  // call to google API for geocoding an address to get latitude and longitude
  private doGetGMGeocoding(address: string, callback: (res: any, error?: any) => void, numberOfRetrys: number = 0) {
    console.log(this.getUrlGMGeocoding(), address , '&key=' , this.GM_API_KEY);
    this.http.get(this.getUrlGMGeocoding() + address + '&key=' + this.GM_API_KEY)
      .retry(numberOfRetrys)
      .subscribe(
        res => callback(res),  // function
        (err: HttpErrorResponse) => callback(undefined, err.error)
      );
  }

}
