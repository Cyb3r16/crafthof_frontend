import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk';
import {UserService} from './user.service';

@Injectable()
export class S3UploadService {

  constructor(private userService: UserService) { }

  fileEvent(fileInput: any): string {
    const AWSService = AWS;
    const region = 'eu-central-1';
    const bucketName = 'crafthof-photos';
    const file = fileInput.target.files[0];
    const accessKey = 'AKIAI5KXJVBGKFQ3ASGA';
    const secretKey = '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';
    // Configures the AWS service and initial authorization
    AWSService.config.update({
      region: region,
      accessKeyId: accessKey,
      secretAccessKey: secretKey,
    });
    // adds the S3 service, make sure the api version and bucket are correct

    let key = '';
    if (this.userService.getUser()) {
      key = this.userService.getUser()._id + '';
    } else {
      key = new Date().toISOString();
    }

    const s3 = new AWSService.S3({
      apiVersion: '2006-03-01',
      params: { Bucket: bucketName}
    });
    s3.upload({ Key: key + '_' + file.name, Bucket: bucketName, Body: file, ACL: 'public-read'},
      function (err, data) {
      if (err) {
        console.log(err, 'there was an error uploading your file');
      }
    });
    return 'https://s3.eu-central-1.amazonaws.com/crafthof-photos/' + key + '_' + file.name;
  }

}
