import { Injectable } from '@angular/core';
import {User} from '../classes/User';
import {BackendService} from './backend.service';
import {Observable} from 'rxjs/Observable';
import {Product} from '../classes/Product';
import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class UserService {

  private user$: ReplaySubject<User> = new ReplaySubject<User>(1);
  private productsFromShop$: ReplaySubject<Product[]> = new ReplaySubject<Product[]>(1);
  private isLoggedIn$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  isShop: boolean;
  private isLoggedIn: boolean;
  private user: User;

  constructor(private backendService: BackendService) {
    this.backendService.getLoggedInUser().subscribe( user => this.user$.next(user), error => this.isLoggedIn$.next(false));
    this.user$.subscribe(user => {
      this.user = user;
      if (user.shop) {
        this.isShop = true;
        this.backendService.getProductsFromShop(user.shop._id).subscribe( products => {
          this.productsFromShop$.next(products);
        });
      } else {
        this.isShop = false;
      }
      this.isLoggedIn$.next(true);
    });
    this.isLoggedIn$.subscribe( state => this.isLoggedIn = state);
  }

  public getUser$(): Observable<User> {
    return this.user$;
  }

  public getProductsFromShop(): Observable<Product[]> {
    return this.productsFromShop$;
  }

  public setLoggedIn(state: boolean) {
    if (state) {
      this.backendService.getLoggedInUser().subscribe( user => this.user$.next(user));
    }
    setTimeout(() => { this.isLoggedIn$.next(state); }, 100);

  }

  public getLoggedIn$(): Observable<boolean> {
    return this.isLoggedIn$;
  }

  public getLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  public getUser(): User {
    return this.user;
  }

  public setNewUserAfterLike(user: User) {
    this.user$.next(user);
  }

  public updateProductsFromShop(shopId) {
    this.backendService.getProductsFromShop(shopId).subscribe(products =>  this.productsFromShop$.next(products));
  }

  public updateUser() {
    this.backendService.getLoggedInUser().subscribe( user => this.user$.next(user));
  }
}
