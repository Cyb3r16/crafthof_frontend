import { Component, OnInit } from '@angular/core';
import {User} from '../classes/User';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {BackendService} from '../services/backend.service';
import {MatDialog} from '@angular/material';
import {ErrorDialogComponent} from '../error-dialog/error-dialog.component';
import {Shop} from '../classes/Shop';
import {S3UploadService} from '../services/s3-upload.service';
import {UserService} from '../services/user.service';
import {GeoCoords} from '../classes/GeoCoords';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  model = new User();

  submitted = false;

  type = 'user';

  constructor(private activatedRoute: ActivatedRoute, private backendService: BackendService,
              private dialog: MatDialog, private s3Upload: S3UploadService, private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      const type = params['type'];
      if (type === 'shop') {
        this.type = type;
        if (!this.model.shop) {
          this.model.shop = new Shop();
        }
      } else {
        this.type = 'user';
      }
    });


  }

  onSubmit() {
    if (this.type === 'user') {
      this.model.shop = undefined;
    }

    // geocode address to get latitude and longitude!
    if (this.type === 'shop') {
      this.backendService.geocodeAddress(this.model.shop.shopAddress, (res, error) => {
        if (error) {
          // this.openDialog(error.message);
          console.log('error', error.message);
        } else {
          this.model.shop.shopLatitude = res.results[0].geometry.location.lat;
          this.model.shop.shopLongitude = res.results[0].geometry.location.lng;
          this.createUserCall();
        }
      });
    } else {
      this.createUserCall();
    }

  }

  private openDialog(error: string) {
    const dialogRef = this.dialog.open(ErrorDialogComponent, {
      data: error
    });
  }

  public fileEventTitle(fileInput: any) {
    this.model.titleImg = this.s3Upload.fileEvent(fileInput);
  }

  public fileEventProfile(fileInput: any) {
    this.model.profileImg = this.s3Upload.fileEvent(fileInput);
  }

  public createUserCall() {
    this.backendService.createUser(this.model, (res, error) => {
      if (error) {
        this.openDialog(error.message);
      } else {
        this.backendService.login(this.model, (res, error) => {
          if (error) {
            this.openDialog(error.message);
          } else {
            this.userService.setLoggedIn(true);
            this.router.navigateByUrl('/');
          }
        });
      }
    });
  }
}
