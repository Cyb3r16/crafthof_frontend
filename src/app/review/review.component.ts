import {Component, Input, OnInit} from '@angular/core';
import {Rating} from '../classes/Rating';
import {Observable} from 'rxjs/Observable';
import {MatDialog} from '@angular/material';
import {ProductService} from '../services/product.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {

  @Input() productId: string;

  ratings$: Observable<Rating[]>;

  constructor(private productService: ProductService,
              private dialog: MatDialog) {
    this.ratings$ = productService.getRatings$();
  }

  ngOnInit() {
    this.productService.getRatingsFromId(this.productId);
  }

}
